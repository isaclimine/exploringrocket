﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalSphere : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Rocket")
        {
            FindObjectOfType<GoalScript>().CompleteOneGoal();
            gameObject.SetActive(false);
        }
    }
}
