﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rock : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Rocket")
        {
            transform.GetChild(0).GetComponent<Rigidbody>().useGravity = true;


            transform.GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
            transform.GetChild(0).GetChild(0).GetComponent<MeshRenderer>().material.color = Color.red;
            transform.GetChild(0).GetChild(1).GetComponent<MeshRenderer>().material.color = Color.red;
            transform.GetChild(0).GetChild(2).GetComponent<MeshRenderer>().material.color = Color.red;
        }
    }
}
