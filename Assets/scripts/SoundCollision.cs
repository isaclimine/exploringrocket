﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundCollision : MonoBehaviour {

    AudioSource m_audioSource;
	void Start () {
        m_audioSource = GetComponent<AudioSource>();
	}

    private void OnCollisionEnter(Collision collision)
    {
        m_audioSource.Stop();
        m_audioSource.volume = collision.relativeVelocity.magnitude * 0.1f;
        m_audioSource.Play();
    }
}
