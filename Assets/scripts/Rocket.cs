﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    [SerializeField] float rcsThrustCurve = 1f;
    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 100f;
    [SerializeField] float thrustInputAcc;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip success;
    [SerializeField] AudioClip death;
    [SerializeField] GameObject impact;

    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem deathParticles;
    [SerializeField] Light m_light;
    [SerializeField] GameObject destruct_version;

    Rigidbody rigidBody;
    AudioSource audioSource;
    Transform m_audioListenerTransform;
    float thrustInput;

    enum State {Alive, Dying, Trancending };
    State state = State.Alive;

    
    private void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        m_audioListenerTransform = FindObjectOfType<AudioListener>().transform;
    }
    void Update()
    {
        if (state == State.Alive)
        {
            RespondToThrustInput();
            RespondToRotateInput();

            m_audioListenerTransform.rotation = Quaternion.Euler(Vector3.zero);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(state != State.Alive)
        {
            return;
        }

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                print("ok");
                break;
            case "Finish":
                StartSuccessSequence();
                break;
            case "MortalCollider":
                StartDeathSequence();
                break;
            case "Box":
                if (collision.relativeVelocity.magnitude > 10)
                {
                    collision.gameObject.GetComponent<Box>().Destruct();
                }
                break;
            default:
                if (collision.relativeVelocity.magnitude > 9)
                {
                    StartDeathSequence();
                }
                else
                {
                    Vector3 point = collision.contacts[0].point;
                    AudioSource impact_Sound = Instantiate(impact, point, Quaternion.identity).GetComponent<AudioSource>();
                    impact_Sound.volume = collision.relativeVelocity.magnitude * 0.1f;
                    Destroy(impact_Sound.gameObject, 1f);

                }
                break;

        }
    }
     
    private void StartDeathSequence()
    {
        state = State.Dying;
        audioSource.Stop();
        audioSource.PlayOneShot(death);
        transform.GetChild(0).gameObject.SetActive(false);
        Instantiate(destruct_version, this.transform.position, this.transform.rotation);
        Invoke("LoadFirstLevel", 2f);
    }
    private void StartSuccessSequence()
    {
        state = State.Trancending;
        audioSource.Stop();
        audioSource.PlayOneShot(success);
        Invoke("NextLevel", 1);
    }

    void LoadFirstLevel()
    {
        int currentlyScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentlyScene);
    }
    void NextLevel()
    {
        int nextScene = SceneManager.GetActiveScene().buildIndex+1;

        //debug
        if (nextScene < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextScene);         
        }
        else
        {
            LoadFirstLevel();
        }
    }
    void RespondToThrustInput()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            ApplyThrust();

            if (thrustInput < 1)
            {
                thrustInput += Time.deltaTime*thrustInputAcc;
            }
        }
        else
        {
            thrustInput = 0;
            audioSource.Stop();
            var emission= mainEngineParticles.emission;
            emission.rateOverTime = 5f;
            m_light.intensity = 5;

            rigidBody.drag = 0.2f;
        }
       
    }
    private void ApplyThrust()
    {
        rigidBody.drag = 0.2f;
        rigidBody.AddRelativeForce(Vector3.up * Mathf.Lerp(0, mainThrust, thrustInput) * Time.deltaTime);
        rigidBody.drag = rcsThrustCurve;

        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
        var emission = mainEngineParticles.emission;
        emission.rateOverTime = Mathf.Lerp(5, 30, thrustInput);
        m_light.intensity = Mathf.Lerp(5, 30, thrustInput);

    }
    void RespondToRotateInput()
    {
        rigidBody.freezeRotation = true;

        float rotationThisFlame = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A))
        {
            rigidBody.MoveRotation(Quaternion.Euler(rigidBody.rotation.eulerAngles + Vector3.forward * rotationThisFlame));
          //  transform.Rotate(Vector3.forward*rotationThisFlame);
        }
        if (Input.GetKey(KeyCode.D))
        {
            rigidBody.MoveRotation(Quaternion.Euler(rigidBody.rotation.eulerAngles - Vector3.forward * rotationThisFlame));
            //   transform.Rotate(-Vector3.forward*rotationThisFlame);
        }

        rigidBody.freezeRotation = false;
        rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
    }
}

