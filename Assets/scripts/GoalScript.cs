﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalScript : MonoBehaviour {
    [SerializeField] int goals;

	void Start () {
        goals = FindObjectsOfType<GoalSphere>().Length;

        if (goals != 0)
        {
            gameObject.tag = "Untagged";
            transform.GetChild(0).gameObject.SetActive(false);
        }

    }

    //public voids
    public void CompleteOneGoal()
    {
        goals--;

        if (goals == 0)
        {
            gameObject.tag = "Finish";
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
