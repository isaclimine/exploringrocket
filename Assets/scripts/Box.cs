﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Box : MonoBehaviour
{
    [SerializeField] GameObject destructVersion;
    public void Destruct()
    {
        Instantiate(destructVersion, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }
}
