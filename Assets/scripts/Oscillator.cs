﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField] float speed;
    [SerializeField] Vector3 movimentVector;
    [SerializeField] AnimationCurve curve;
    [SerializeField] [Range(0, 1)] float Factor = 1;

    [Header("Rotation")]
    [SerializeField] Vector3 rotVector;


    Quaternion startRotation;
    Vector3 startPosition;
    bool toggleFactor = false;
	// Use this for initialization
	void Start () {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }
	
	// Update is called once per frame
	void Update () {
        if (toggleFactor)
        {
            Factor += speed * Time.deltaTime;
                if (Factor > 1)
                    toggleFactor = false;
        }
        else
        {
            Factor -= speed * Time.deltaTime;
            if (Factor < 0)
                toggleFactor = true;
        }

        Vector3 offSet = movimentVector * curve.Evaluate(Factor);
        transform.position = startPosition + offSet;


        Quaternion RotOffSet =Quaternion.Euler( rotVector * curve.Evaluate(Factor));

        transform.rotation = startRotation * RotOffSet;

    }
}
